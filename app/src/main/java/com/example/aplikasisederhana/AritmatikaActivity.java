package com.example.aplikasisederhana;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class AritmatikaActivity extends AppCompatActivity {
    TextView tvAngka1,tvAngka2,tvMasukkan,tvOpr;
    Button btnCek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aritmatika);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvAngka1= findViewById(R.id.tvAngka1);
        tvAngka2 = findViewById(R.id.tvAngka2);
        tvMasukkan = findViewById(R.id.edtAns);
        tvOpr = findViewById(R.id.tvOperasi);
        btnCek = findViewById(R.id.btnAns);

        String getData = getIntent().getStringExtra("angka");
        if (getData.equals("1")){
            getSupportActionBar().setTitle("Penjumlahan");
            resetTambah();

            btnCek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    if (tvMasukkan.getText().toString().length() > 0){
                        int num1 = Integer.valueOf(tvAngka1.getText().toString());
                        int num2 = Integer.valueOf(tvAngka2.getText().toString());
                        int num3 = Integer.valueOf(tvMasukkan.getText().toString());

                        int jumlah = num1 + num2;
                        if (jumlah == num3){
                            Toast.makeText(AritmatikaActivity.this,"Jawaban Benar",Toast.LENGTH_SHORT).show();
                            resetTambah();
                            tvMasukkan.setText("");
                        }else {
                            Toast.makeText(AritmatikaActivity.this,"Jawaban Salah, silahkan masukkan jawaban yang Benar",Toast.LENGTH_SHORT).show();

                        }
                    }else {
                        Toast.makeText(AritmatikaActivity.this,"Inputan Kosong",Toast.LENGTH_SHORT).show();
                    }





                }

            });

        }else{
            getSupportActionBar().setTitle("Pengurangan");
            resetKurang();
            btnCek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    if (tvMasukkan.getText().toString().length() > 0){
                        int num1 = Integer.valueOf(tvAngka1.getText().toString());
                        int num2 = Integer.valueOf(tvAngka2.getText().toString());
                        int num3 = Integer.valueOf(tvMasukkan.getText().toString());

                        int jumlah = num1 - num2;
                        if (jumlah == num3){
                            Toast.makeText(AritmatikaActivity.this,"Jawaban Benar",Toast.LENGTH_SHORT).show();
                            resetKurang();
                            tvMasukkan.setText("");
                        }else {
                            Toast.makeText(AritmatikaActivity.this,"Jawaban Salah, silahkan masukkan jawaban yang Benar",Toast.LENGTH_SHORT).show();

                        }

                    }else {
                        Toast.makeText(AritmatikaActivity.this,"Inputan Kosong",Toast.LENGTH_SHORT).show();
                    }





                }

            });

        }



    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void resetTambah (){
        Random random = new Random();
        int randomNum1 = random.nextInt(100-1) + 1;

        int randomNum2 = random.nextInt(100-1) + 1;


        tvAngka1.setText(String.valueOf(randomNum1));
        tvAngka2.setText(String.valueOf(randomNum2));
        tvOpr.setText("+");



    }
    private void resetKurang (){
        Random random = new Random();
        int randomNum1 = random.nextInt(500-50) + 50;

        int randomNum2 = random.nextInt(100-1) + 1;


        tvAngka1.setText(String.valueOf(randomNum1));
        tvAngka2.setText(String.valueOf(randomNum2));
        tvOpr.setText("-");



    }
}