package com.example.aplikasisederhana;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Tambah(View view) {
        Intent i = new Intent(MainActivity.this,AritmatikaActivity.class);
        i.putExtra("angka","1");
        startActivity(i);
    }

    public void Kurang(View view) {
        Intent i = new Intent(MainActivity.this,AritmatikaActivity.class);
        i.putExtra("angka","2");
        startActivity(i);
    }
}